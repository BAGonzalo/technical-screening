# #!/usr/bin/env python3
import json
import pandas as pd
import numpy as np
import cv2
import itertools
import json
import os

def open_json_from_file(json_path):
    """
    Loads a json from a file path.

    :param json_path: path to the json file
    :return: the loaded json
    """
    try:
        with open(json_path) as json_file:
            json_data = json.load(json_file)
    except:
        print("Could not open file %s in json format." %json_path)
        raise

    return json_data

def save_json_to_file(json_data, json_path):
    """
    Saves a json to a file.

    :param json_data: the actual json
    :param json_path: path to the json file
    :return:
    """
    try:
        with open(json_path, 'w') as json_file:
            json.dump(json_data, json_file)
    except:
        print("Could not save file %s in json format." %json_path)
        raise

    return

def pretty_print(inline_json):
    """
    Prints a json in the command interface in easily-readable format.

    :param inline_json:
    :return:
    """
    print(json.dumps(inline_json, indent=4, sort_keys=True))
    return

# implemented functions
"""
There are faster libraries to compute polygon intersections, such us shapely, geopandas, etc. but I opted for implementing the required funtions at them this time.
"""

def get_i_area(boxA, boxB, margin=0):
    # coordinates of the intersection polygon
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])
 
    # area
    i_area = max(0, xB - xA + margin) * max(0, yB - yA + margin)
    return i_area

def get_t_area(boxA, boxB):

    boxA_area = (boxA[2] - boxA[0]) * (boxA[3] - boxA[1])
    boxB_area = (boxB[2] - boxB[0]) * (boxB[3] - boxB[1])

    return boxA_area + boxB_area

def get_iou(boxA, boxB):
    i_area = get_i_area(boxA, boxB)
    t_area = get_t_area(boxA, boxB)
    
    iou = i_area / float(t_area - i_area)
    
    return round(iou, 5)

def plot_im(im, title=None):
    nrows, ncols = 1, 1
    fig, ax = plt.subplots(nrows=nrows, ncols=ncols)
    fig.set_size_inches(10, 10)
    fig.tight_layout()
    if title is not None: plt.title(title)
    ax.imshow(im) 
    
def get_bb_list(im, bb_data):
        bb_list = []
        # for bb_type, bb_data in data.items(): 
        for bboxs in bb_data:
            coor = bboxs['region']
            # bottom left corner (lower corner)
            lc = (int(coor['xmin']*im.shape[1]), int(coor['ymin']*im.shape[0]))
            # upper right corner (higer corner)
            hc = (int(coor['xmax']*im.shape[1]), int(coor['ymax']*im.shape[0]))
            bbox = [lc[0], lc[1], hc[0], hc[1]]
            bb_list.append(bbox)
        return bb_list

def get_bb_pairs(annotations, predictions):
    """
    Get all possible combinations of (groundtruth, prediction) bbox pairs
    """
    return list(itertools.product(annotations, predictions))

def get_intersected_bb_pairs(pairs, iou_thres=0.8):
    """
    pairs: all possible combinations of (groundtruth, prediction) bbox pairs
    TODO: If two predicted boxes satisfy the IoU criterion, 
    only the one with the highest score will be associated with the ground truth box.
    """
    # intersected pairs
    i_pairs = {'p': [], 'g': []}

    for i, j in pairs:
        if i in i_pairs['p'] or j in i_pairs['g']: 
            continue
        i_area, t_area, iou = get_i_area(i, j), get_t_area(i, j), get_iou(i, j)

        if iou  >  iou_thres:
            i_pairs['p'].append(i)
            i_pairs['g'].append(j)
            
    return i_pairs
    
def get_rates(annotations, predictions, i_pairs):
    """
    i_pairs: intersected pairs
    """
    TP_g, TP_p, FP, FN = 0, 0, 0, 0

    for i in annotations:
        if i in i_pairs['p']: TP_g += 1
        else: FN += 1
    for j in predictions:
        if j in i_pairs['g']: TP_p += 1
        else: FP += 1    
    assert TP_g == TP_p

    return TP_g, FN, FP
    
def evaluate_images(annotations, predictions, threshold=0, Jaccard_min=0.5):
    """
    Take a list of annotations and predictions, a threshold, and returns the number of true positives, false positives,
    and false negatives.
    :param annotations: the json containing the annotations
    :param predictions: the json containing the predictions
    :param threshold: the threshold used to select the boxes to evaluate
    :param Jaccard_min: the IoU threshold used to evaluate
    :return: (true_positives, false_negatives, false_positives)
    """

    pairs = get_bb_pairs(annotations, predictions)
    i_pairs = get_intersected_bb_pairs(pairs, iou_thres=Jaccard_min)
    true_positives, false_negatives, false_positives = get_rates(annotations, predictions, i_pairs)

    return true_positives, false_negatives, false_positives


if __name__ == '__main__':
    # Load annotations from json file
    groundtruth = open_json_from_file('groundtruth.json')
    predictions = open_json_from_file('predictions.json')
    
    col_dic = {'g': (200, 0, 0), 'p': (0, 0, 255)}
        
    out_p = 'output/' # output path
    if not os.path.exists(out_p): os.makedirs(out_p)  
        
    p_raw_data = {i['location']: i['annotated_regions'] for i in predictions['images']}
    
    # Evaluate
    for img in groundtruth['images']:
        
        im_p = img['location'] # img path

        try: p_raw = p_raw_data[im_p]
        except: 
            print('No groundtruth data found for image %s' %im_p) 
            continue
            
        g_raw = img['annotated_regions']
        p_raw = p_raw_data[im_p]
        
        try: 
            im = cv2.imread(im_p, cv2.IMREAD_UNCHANGED)
            im = im[:,:,[2,1,0]].copy()
        except: 
            print('No image found for file name %s in %s' %(im_p, im_p))
            continue
        
        bb_list = {'g': get_bb_list(im, g_raw), 'p': get_bb_list(im, p_raw)}
        
        for bb_type, data in bb_list.items():
            for bbox in data:
                cv2.rectangle(im, (bbox[0], bbox[1]), (bbox[2], bbox[3]), col_dic[bb_type], int(3e-3*im.shape[1]))
        
        cv2.imwrite(out_p +  im_p.split('/')[1].split('.')[0] + '.png', im[:,:,[2,1,0]])
        
        TP, FN, FP = evaluate_images(bb_list['g'], bb_list['p'])
#         plot_im(im, title = "Image '%s:   TP: %s - FN: %s - FP: %s   |   Total IOU: " %(img['location'], TP, FN, FP))
        print("Image '%s'\n\t- TP: %s\n\t- FN: %s\n\t- FP: %s" %(img['location'], TP, FN, FP))