# Solution Architect Challenge

This repo contains the proposed solutions for the following [challenge](https://github.com/Deepomatic/technical-screening/archive/master.zip) composed of the exercises:

#### Exercise 1: Axle Tree
The purpose of this simple exercise is to do some basic bounding box manipulation, grouping, verifying, etc. For more information simply go into the `1_axle_tree_exercise` directory and open the `README.md` file.

#### Exercise 2: Precision Accuracy 
This time you are given both the model prediction and the ground truth for the images and you need to build a module that evaluates the performance of the network. Same thing, just go to `2_precision_accuracy/README.md`.

Addittional details are provided in each exercise folders' README.md file.


The solution for each of the exercise is delivered as: (1) an .py file (to run them see below), (2) an .ipynb file called ```playground``` containing same code and visualizations (3) 'output' folder with the annotated images. 

**Exercise 2**: There was inconsistency between the extension of some of the images, and its names in the groundtruth and predictions .json files - .JPEG vs. png. Some of the actual extensions have been changed to align them to the annotation files (so it is suggested to use the 'planes' folder contained in this repository instead of the original one).

In global, there is scope for code optimization.

## Setting up

To run the code and notebooks, use the image dockerfile provided [CPU-only support] as described in prerequisites.

[1] Install [Docker Engine](https://www.docker.com/community-edition#/download); and clone the repository.

[2] Build the image, and run the environment by:

```
cd technical-screening
docker build -t deepo -f dockerfile .
docker run -it -v {global-path-to-repo}/technical-screening:/technical-screening -p 9000:9000 --user root --shm-size 16G --name deepo deepo
cd technical-screening 
jupyter notebook --ip 0.0.0.0 --no-browser --port 9000 --allow-root 

```

To enter the running container from another console window, run:

```
docker exec -it deepo bin/bash
```

[3] Access the Jupyter notebook tree at http://localhost:9000/, and enter the token provided by the console -e.g.:

```
[I 13:30:53.425 NotebookApp] The Jupyter Notebook is running at:
[I 13:30:53.426 NotebookApp] http://809b15d36bae:9000/?token=5b849c9e10fc2671579c113e270f3953ad33113120ce28d6
```

The repo will be visualised as a files tree in your browser.

[4] Open each individual notebook, and run it if desired.
[5] The solutions can also be outputted by running:

```
cd 1_axle_tree_exercise
python3.5 merge_axle_tree.py
```

```
cd 2_precision_accuracy
python3.5 evaluate_perf.py
```

## Authors

* **BAGonzalo** - https://gitlab.com/BAGonzalo (bagonzalo@gmail.com)